/* COMP10050 - Software Engineering Project 1 */

/* Ilya Lyubomirov : 16205331 */
/* Mikhail Yankelevich : 16205326 */

/* This program version initialises necessary game components for the Othello/Reversi Game.
   Currently, it can ask users for their names/nicknames, assign the colors to players and
   print the board. This version also briefly explains the main rules of the game. */

/* Defines libraries, structs, constants, global variables, and prototypes of functions */
#include <stdio.h>
#include <string.h>

/* Player Struct */
typedef struct Player {
  char name[20];//name of the player
  int diskType;//B = 1 && W = 2
  int diskNumber;//number of disks of chosen color on the board
}Player;

/* Disk struct */
typedef struct Disks {
  int diskType;//B = 1 && W = 2
  int posX;//column position of the disk
  int posY;//row position of the disk
}Disk;

/* Function Prototypes */
void showRules(void);
void playerRecord(struct Player *player1, struct Player *player2);
void printBoard(struct Player *player1, struct Player *player2, int *remainingCells, int board[8][8]);
void BoardSetup(struct Player *player1, struct Player *player2, int *remainingCells, int board[8][8]);


int main(void)//Main
{
  printf("Software Engineering Project 1 (16205331 & 16205326)\n");
  
  struct Player player1;//initialises 1st player
  struct Player player2;//initialises 2nd player
  int remainingCells = 64;//number of empty cells on the board (by default 64 empty cells)
  int board[8][8];//playing board (8 by 8)

  showRules();//calls the function which prints the rules
  BoardSetup(&player1, &player2, &remainingCells, board);//sets up the initial state of the board for the game 
  playerRecord(&player1, &player2);//calls the function which records players' names
  printBoard(&player1, &player2, &remainingCells, board);//prints board status for the beginning of the game 

  return 0;
}

/* This function explains the basic rules of the game and symbols used. */
void showRules(void)
{
  /* Brief rules, explanations of symbols */
  puts("\n======================== GAME RULES ========================\n");
  puts("Othello/Reversi Game\n");
  puts("Playing Board: 8 by 8 (64 cells). W stands for White Disks, B stands for Black Disks, _ is an empty cell\n");
  puts("Each player can move 1 disk at a time in turns. Each player (i.e. Black) can only move in such a position that there exists at least one straight (horizontal, vertical, or diagonal) occupied line between the new piece and another Black piece, with one or more contiguous White pieces between them.\n");
  puts("By default there are 4 disks of 2 colors in the middle.\nPlayer using Black disks moves first!");
  puts("\n======================== GAME RULES ========================\n");
  
  return; 
}


/* This function asks for player names and assigns a disk color to each player */
void playerRecord(struct Player *player1, struct Player *player2)
{
  /* Asks for first player info */
  printf("\nPlease enter player 1 name: ");
  scanf("%s", player1->name);//records player 1 name
  
  player1->diskType = 1;//assigns 1 (or Black) color to player 1
  printf("%s is assigned color Black\n", player1->name);
  
  /* Asks for second player info */
  printf("\nPlease enter player 2 name: ");
  scanf("%s", player2->name);//records player 2 name
  
  player2->diskType = 2;//assigns 2 (or White) color to player 2
  printf("%s is assigned color White\n\n", player2->name);

  if(strcmp(player1->name, player2->name) == 0)//alerts if players entered the same names
  {
  	puts("--> Be aware that player names are the same! <--\n");
  }

  return;
}


/* This function sets the default state of the board (positions of the initial disks) */
void BoardSetup(struct Player *player1, struct Player *player2, int *remainingCells, int board[8][8])
{
  int i, j;

  /* Fills the whole board with empty spaces */
  for(i = 0; i < 8; i++)
  {
  	for(j = 0; j < 8; j++)
  	{
  		board[i][j] = 0;
  	}
  }

  /* Fills the initial positions of the disks  */
  board[3][3] = 2;//1st white disk position
  board[4][4] = 2;//2nd white disk pos
  board[3][4] = 1;//1st black disk pos
  board[4][3] = 1;//2nd black disk 

  *remainingCells = *remainingCells - 4;//number of empty cells is decreased by 4 (which are occupied by default)

  player1->diskNumber = 2;//player 1 has two black disks on the board
  player2->diskNumber = 2;//player 2 has two white disks on the board

  return; 
}


/* This function outputs the board according to the received disk positions */
void printBoard(struct Player *player1, struct Player *player2, int *remainingCells, int board[8][8])
{
  int i, j;

  printf("| |1|2|3|4|5|6|7|8");//prints the top board border

  for (i = 0; i < 8; i++)
  {
    printf("|\n|%d",i + 1);//for each row prints the side borders
    
    for (j = 0; j < 8; j++)
      {
        if(board[i][j] == 0)//if no disks are in the cell
          {
            printf("|_");//places _ there
          }
          else if(board[i][j] == 1)//if received color is black (1)
          {
            printf("|B");//puts B in the cell
          }
          else if(board[i][j] == 2)//if received white (2)
          {
            printf("|W");//puts W in the cell
          }
      }
  }
  printf("|\n");

  printf("\nNumber of empty cells: %d\n", *remainingCells);//pints number of empty sells
  printf("Black Disks: %d\n", player1->diskNumber);//prints number of player 1 disks 
  printf("White Disks: %d\n", player2->diskNumber);//prints number of player 2 disks 

  return;
}
