COMP10050 Assignment 2

Ilya Lyubomirov : 16205331
Mikhail Yankelevich : 16205326

-Mac OS X GCC compiler-


To satisfy the requirements of this assignment we have decided to implement a couple of functions, structs, and variables which will be useful in the future.

Firstly, we have implemented two structs. One of them was done by Mikhail and is used to track Players (their name, color of the disk, and number of the disks of this color on the board), while the other was done by Ilya and is used for tracking Disks (color of the disk and its X & Y position on the board).

Secondly, we have implemented the player name input. Mikhail's part asks the names, and Ilya's part is assigning the colours of the disks to the players. At this stage Ilya had changed the diskType (colour of the disk assigned to player) from 'char' to 'int' so that we will not have to use the string library to scan and copy the values (instead of using B and W, we use 1 and 2 to represent colours in the program. Empty cells count as 0).

Then, Mikhail has implemented a printBoard function (outputs the visual state of the board) while Ilya has implemented the showRules function (shows the rules/board look before the start of the game). 

The 4 initial disks are initialised in the BoardSetup function (called from showRules) so that they will be assigned once in the program cycle. 

After some minor changes (syntax, comments, code structure) were refined the program can show the rules of this game, read users' inputs, assign colours to the players, and print the playing board.

The functions were reconstructed upon clarification. Important functions now receive pointers from the main(void) instead of utilising global variables. Structs and Arrays were moved from global to pointers.

Function which records player names now shows an alert if users have entered the same name.